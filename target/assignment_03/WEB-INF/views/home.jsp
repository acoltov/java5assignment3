<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
  <head>
    <title>TeamManagement</title>
    <link rel="stylesheet" 
          type="text/css" 
          href="<c:url value="/resources/style.css" />" >
  </head>
  <body>
    <h1>Welcome to Team Management</h1>


    <table>
      <tr>
        <td align="center">
          <h3>Player</h3>
        </td>
        <td align="center">
          <h3>Trainer</h3>
        </td>
      </tr>
      <tr>
        <td align="center">
          <a href="<c:url value="/Player/create" />">Create Player</a>
        </td>
        <td align="center">
          <a href="<c:url value="/Trainer/create" />">Create Trainer</a>
        </td>
      </tr>
      <tr>
        <td align="center">
          <a href="<c:url value="/Player/allPlayers" />">Shoe All Player</a>
        </td>
        <td align="center">
          <a href="<c:url value="/Trainer/allTrainers" />">Shoe All Trainer</a>
        </td>
      </tr>
      <tr>
        <td align="center">
          <a href="<c:url value="/Player/displayPlayerUsingREST" />">Show Player REST</a>
        </td>
        <td align="center">
          <a href="<c:url value="/Trainer/displayTrainerUsingREST" />">Show Trainer REST</a>
        </td>
      </tr>
      <tr>
        <td align="center">
          <a href="<c:url value="/PlayerREST/all" />">Show All Player REST</a> &nbsp; &nbsp; &nbsp;
        </td>
        <td align="center">
          &nbsp; &nbsp; &nbsp; <a href="<c:url value="/TrainerREST/all" />">Show All Trainer REST</a>
        </td>
      </tr>
    </table>


    <%--<a href="<c:url value="/Player/create" />">Create Player</a> |--%>
    <%--<a href="<c:url value="/Trainer/create" />">Create Trainer</a> |--%>
    <%--<a href="<c:url value="/createTeam" />">Create Team</a>--%>
  </body>
</html>
