package com.andriancoltov.web;

import com.andriancoltov.api.PlayerApiController;
import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Statistics;
import com.andriancoltov.data.PlayerRepository;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by Alienware on 2015-08-10.
 */

public class PlayerControllerTest {

  @Test
  public void testPlayerCreatePage() throws Exception {
    PlayerRepository mockRepository = mock(PlayerRepository.class);
    PlayerController controller = new PlayerController(mockRepository);
    MockMvc mockMvc = standaloneSetup(controller).build();
    mockMvc.perform(get("/Player/create"))
           .andExpect(view().name("createPlayerForm"));
  }

//    @Test
//    public void testPlayerShowRestPage() throws Exception {
//        PlayerRepository mockRepository = mock(PlayerRepository.class);
//        PlayerApiController controller = new PlayerApiController(mockRepository);
//        MockMvc mockMvc = standaloneSetup(controller).build();
//        mockMvc.perform(get("/PlayerREST/all"))
//                .andExpect(view().name("PlayerREST/all"));
//    }

  @Test
  public void testPlayerShowPage() throws Exception {
      PlayerRepository mockRepository = mock(PlayerRepository.class);
    PlayerController controller = new PlayerController(mockRepository);
    MockMvc mockMvc = standaloneSetup(controller).build();

    mockMvc.perform(post("/Player/create")
           .param("firstName", "Super")
           .param("lastName", "Maaaan")
           .param("age", "22")
           .param("countryOfBirth", "Canada")
           .param("position", "goalkeeper")
           .param("annualSalary", "250000")
           .param("numberOfGoals", "3")
           .param("numberOfBookings", "2")
            )
            .andExpect(redirectedUrl("/Player/Maaaan"));
  }

}
