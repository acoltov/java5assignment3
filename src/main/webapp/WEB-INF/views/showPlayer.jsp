<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" session="false" %>
<html>
<head>
    <title>Player Details</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />" >
</head>
<body>
<h1>Player Details</h1>
<%--<table border=0 cellpadding=0>--%>
    <%--<tr>--%>
        <%--<td>First Name (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.firstName}--%>
                             <%--${playerDefender.firstName}--%>
                             <%--${playerMidfielder.firstName}--%>
                             <%--${playerForward.firstName}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Last Name (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.lastName}--%>
                             <%--${playerDefender.lastName}--%>
                             <%--${playerMidfielder.lastName}--%>
                             <%--${playerForward.lastName}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Age (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.age}--%>
                             <%--${playerDefender.age}--%>
                             <%--${playerMidfielder.age}--%>
                             <%--${playerForward.age}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Country of birth (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.countryOfBirth}--%>
                             <%--${playerDefender.countryOfBirth}--%>
                             <%--${playerMidfielder.countryOfBirth}--%>
                             <%--${playerForward.countryOfBirth}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Position (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.position}--%>
                             <%--${playerDefender.position}--%>
                             <%--${playerMidfielder.position}--%>
                             <%--${playerForward.position}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Annual salary (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.annualSalary}--%>
                             <%--${playerDefender.annualSalary}--%>
                             <%--${playerMidfielder.annualSalary}--%>
                             <%--${playerForward.annualSalary}--%>
                              <%--${playerGoalkeeper.currency}--%>
                              <%--${playerDefender.currency}--%>
                              <%--${playerMidfielder.currency}--%>
                              <%--${playerForward.currency}--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td>Stats (*):</td>--%>
        <%--<td><c:out value="--%>
                             <%--${playerGoalkeeper.statistics.numberOfGoals}--%>
                             <%--${playerDefender.statistics.numberOfGoals}--%>
                             <%--${playerMidfielder.statistics.numberOfGoals}--%>
                             <%--${playerForward.statistics.numberOfGoals}--%>
                             <%--goals--%>
                             <%--${playerGoalkeeper.statistics.numberOfBookings}--%>
                             <%--${playerDefender.statistics.numberOfBookings}--%>
                             <%--${playerMidfielder.statistics.numberOfBookings}--%>
                             <%--${playerForward.statistics.numberOfBookings}--%>
                             <%--bookings--%>
                             <%--" />--%>
        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td colspan="2">&nbsp</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td colspan="2" align="center"><button type="button" onclick="document.location='/assignment_02/'">Return</button></td>--%>
    <%--</tr>--%>
<%--</table>--%>
<table border=0 cellpadding=0>
    <c:forEach items="${playerContainer}" var="playerContainer">
    <%--<%for (int i = ; )items="${playerContainer}" var="playerContainer">--%>
        <tr>
            <td>First Name (*):</td>
            <td><c:out value="${playerContainer.firstName}" /></td>
        </tr>
        <tr>
            <td>Last Name (*):</td>
            <td><c:out value="${playerContainer.lastName}" /></td>
        </tr>
        <tr>
            <td>Age (*):</td>
            <td><c:out value="${playerContainer.age}" /></td>
        </tr>
        <tr>
            <td>Country of birth (*):</td>
            <td><c:out value="${playerContainer.countryOfBirth}" /></td>
        </tr>
        <tr>
            <td>Position (*):</td>
            <td><c:out value="${playerContainer.position}" /></td>
        </tr>
        <tr>
            <td>Annual salary (*):</td>
            <td><c:out value="${playerContainer.annualSalary} ${playerContainer.currency}" /></td>
        </tr>
        <tr>
            <td>Stats (*):</td>
            <td><c:out value="${playerContainer.numberOfGoals} goals ${playerContainer.numberOfBookings} bookings" /></td>
        </tr>
        <tr>
            <td colspan="2" align="center">####################################</td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="2">&nbsp</td>
    </tr>
    <tr>
        <td colspan="2" align="center"><button type="button" onclick="document.location='/'">Return</button></td>
    </tr>
</table>
</body>
</html>
