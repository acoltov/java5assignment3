<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ page isELIgnored="false" session="false" %>
<html>
<head>
    <title>Trainer Details REST</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />" >
</head>
<body>

<h1>Display Trainer using REST by First and Last Name</h1>
<sf:form method="POST" modelAttribute="trainerContainer">
  <sf:errors path="*" element="div" cssClass="errors"/>
  <table border="0" cellspacing="2">
    <tr>
      <td>First Name (*):</td>
      <td><input type="text" name="firstName"/><br/></td>
    </tr>
    <tr>
      <td>Last Name (*):</td>
      <td><input type="text" name="lastName"/><br/></td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <input type="reset" value="Reset"/> |
        <button type="button" onclick="document.location='/'">Cancel</button> |
        <input type="submit" value="Show Trainer REST"/>
      </td>
    </tr>
  </table>
</sf:form>

</body>
</html>
