package com.andriancoltov.api;

import com.andriancoltov.data.JdbcPlayerRepository;
import com.andriancoltov.data.PlayerNotFoundException;
import com.andriancoltov.data.PlayerRepository;
import com.andriancoltov.web.PlayerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Alienware on 2015-08-26.
 */

@RestController
@RequestMapping("/PlayerREST")
public class PlayerApiController {

    public PlayerRepository playerRepository;

    @Autowired
    public PlayerApiController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @RequestMapping(value = "/{lastName}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> showPlayerByLastName(@PathVariable String lastName) {
        System.out.println();
        System.out.println("LN Rest Player: "  + lastName);
        System.out.println();
        try {
            List<PlayerContainer> playerContainer = playerRepository.findPlayerByLastName(lastName);
            System.out.println(playerContainer);
            if (playerContainer.size() > 0) {
                return new ResponseEntity<List<PlayerContainer>>(playerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "Player with last name [" + lastName + "] not found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
        } catch (PlayerNotFoundException e) {
            Error error = new Error(1, "Player with last name [" + lastName + "] not found");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> showAllPlayers() {
        try {
            List<PlayerContainer> playerContainer = playerRepository.findAllPlayers();
            System.out.println(playerContainer);
            if (playerContainer.size() > 0) {
                return new ResponseEntity<List<PlayerContainer>>(playerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "No players found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
        } catch (PlayerNotFoundException e) {
            Error error = new Error(1, "No players found");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }

}
