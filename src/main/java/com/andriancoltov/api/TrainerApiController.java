package com.andriancoltov.api;

/**
 * Created by Alienware on 2015-08-26.
 */

import com.andriancoltov.data.TrainerNotFoundException;
import com.andriancoltov.data.TrainerRepository;
import com.andriancoltov.web.TrainerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/TrainerREST")
public class TrainerApiController {

    private TrainerRepository trainerRepository;

    @Autowired
    public TrainerApiController(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    @RequestMapping(value="/{firstNameAndLastName}", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showTrainerByFirstLastName(@PathVariable String firstNameAndLastName) {

        String firstName = firstNameAndLastName.split("_")[0];
        String lastName = firstNameAndLastName.split("_")[1];
        System.out.println("FN_LN Rest Trainer: " + firstName + " " + lastName + " " + firstNameAndLastName);

        try {
            List<TrainerContainer> trainerContainer = trainerRepository.findTrainerByFirstAndLastName(firstName, lastName);

            System.out.println(trainerContainer.toString());

            if (trainerContainer.size() > 0) {
                return new ResponseEntity<List<TrainerContainer>>(trainerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "Trainer with first and last name [" + firstName + " " + lastName + "] not found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
            } catch (TrainerNotFoundException e) {
                Error error = new Error(1, "Trainer with first and last name [" + firstName + " " + lastName + "] not found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
    }

    @RequestMapping(value="/all", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> showAllTrainers() {

        try {
            List<TrainerContainer> trainerContainer = trainerRepository.findAllTrainers();

            System.out.println(trainerContainer.toString());

            if (trainerContainer.size() > 0) {
                return new ResponseEntity<List<TrainerContainer>>(trainerContainer, HttpStatus.OK);
            } else {
                Error error = new Error(1, "No trainers found");
                return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
            }
        } catch (TrainerNotFoundException e) {
            Error error = new Error(1, "No trainers found");
            return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
        }
    }


}
