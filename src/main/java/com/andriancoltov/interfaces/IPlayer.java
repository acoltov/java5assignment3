package com.andriancoltov.interfaces;

import com.andriancoltov.playerEnumerator.PlayerType;

/**
 * Created by Alienware on 2015-07-27.
 */

public interface IPlayer {

    PlayerType position = null;

    PlayerType getPosition();

}
