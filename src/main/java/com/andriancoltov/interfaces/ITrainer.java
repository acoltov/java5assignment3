package com.andriancoltov.interfaces;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by Alienware on 2015-07-27.
 */
public interface ITrainer {

    String firstName = null;
    String lastName = null;
    int age = 0;
    BigDecimal annualSalary = null;
    Currency currency = null;
}
