package com.andriancoltov.factory;

import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Team;
import com.andriancoltov.beans.Trainer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Alienware on 2015-07-26.
 */

@Component
public class FactoryTeam {

    private List<Player> playersList;
    private Trainer trainer;
    private String teamName;
    private int foundationYear;

    public Team createTeam(List<Player> playersList, Trainer trainer, String teamName, int foundationYear) {
        this.playersList = playersList;
        this.trainer = trainer;
        this.teamName = teamName;
        this.foundationYear = foundationYear;

        if (isValidTeam()) {
            return new Team(this.playersList, this.trainer, this.teamName, this.foundationYear);
        } else {
            System.out.println("Team details are incorrect so team not created\n");
            return null;
        }
    }


    //function to validate the team
    private boolean isValidTeam() {

        if (isValidPlayersList() && isValidTrainer() && isValidStr(teamName) && isValidYearOfFoundation()) {
            return true;
        } else {
            System.out.println("Invalid team");
            return false;
        }
    }

    //function to validate playerList
    private boolean isValidPlayersList() {
        if (playersList.size() == 22) {
            return true;
        } else {
            System.out.println("Invalid team playersList");
            return false;
        }
    }

    //function to validate the trainer
    private boolean isValidTrainer() {
        if (trainer != null) {
            return true;
        } else {
            System.out.println("Invalid team trainer");
            return false;
        }
    }

    //function to validate the teamName
    private boolean isValidStr(String str) {
        if (!str.isEmpty() || str != "") {
            return true;
        } else {
            System.out.println("Invalid team teamName");
            return false;
        }
    }

    private boolean isValidYearOfFoundation() {
        if (foundationYear >= 1950) {
            return true;
        } else {
            System.out.println("Invalid team foundationYear");
            return false;
        }
    }
}
