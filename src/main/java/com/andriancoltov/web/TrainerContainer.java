package com.andriancoltov.web;


import org.springframework.stereotype.Component;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by Alienware on 2015-07-23.
 */

@Component
public class TrainerContainer {

    @NotNull
    @Size(min=3, max=16, message = "{firstName.size}")
    @Pattern(regexp = "^[A-Za-z ]++$", message = "{firstName.regexp}")
    private String firstName;

    @NotNull
    @Size(min=3, max=16, message = "{lastName.size}")
    @Pattern(regexp = "^[A-Za-z ]++$", message = "{lastName.regexp}")
    private String lastName;

    @NotNull
    @Min(value = 40, message = "{age.trainer}")
    private int age;

    @NotNull
    @Min(value = 90000,message = "{salary.size}")
    private BigDecimal annualSalary;

    private Currency currency = Currency.getInstance(Locale.US);

    public TrainerContainer() {
    }

    public TrainerContainer(String firstName, String lastName, int age, BigDecimal annualSalary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.annualSalary = annualSalary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TrainerContainer)) return false;
        TrainerContainer that = (TrainerContainer) o;
        return Objects.equals(age, that.age) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(annualSalary, that.annualSalary) &&
                Objects.equals(currency, that.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, annualSalary, currency);
    }

    @Override
    public String toString() {
        return "TrainerContainer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", annualSalary=" + annualSalary +
                '}';
    }
}
