package com.andriancoltov.web;

import com.andriancoltov.beans.Player;
import com.andriancoltov.beans.Statistics;
import com.andriancoltov.data.PlayerRepository;
import com.andriancoltov.factory.FactoryPlayer;
import com.andriancoltov.playerEnumerator.PlayerType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/Player")
public class PlayerController {

  // non database case
//  private PlayerContainer playerContainer;
//  private Player player;
//  private Currency currency = Currency.getInstance(Locale.US);
//
//  @Autowired
//  private FactoryPlayer factoryPlayer;
//
//  @Autowired
//  public PlayerController(PlayerContainer playerContainer) {
//    this.playerContainer = playerContainer;
//  }
//
//  @RequestMapping(value="/create", method=GET)
//  public String showCreatePlayerForm(Model model) {
//    model.addAttribute(new PlayerContainer());
//    return "createPlayerForm";
//  }
//
//  @RequestMapping(value="/create", method=POST)
//  public String processCreatePlayer(
//      @Valid PlayerContainer playerContainer,
//      Errors errors) {
//    if (errors.hasErrors()) {
//      return "createPlayerForm";
//    }
//
////    spitterRepository.save(spitter);
//
//    player = factoryPlayer.createPlayer(playerContainer.getFirstName(), playerContainer.getLastName(),
//            playerContainer.getAge(), playerContainer.getCountryOfBirth(), playerContainer.getPosition(),
//            playerContainer.getAnnualSalary(), currency,
//            new Statistics(playerContainer.getNumberOfGoals(), playerContainer.getNumberOfBookings()));
//
////    System.out.println(player.toString());
//
//    if (player == null) {
//      return "createPlayerForm";
//    }
//    else {
//      return "redirect:/Player/" + player.getPosition();
//    }
//  }
//
//  @RequestMapping(value="/{position}", method=GET)
//  public String showCreatedPlayer(@PathVariable PlayerType position, Model model) {
//    model.addAttribute(player);
//
//    System.out.println(model.toString());
//
//    return "showPlayer";
//  }





  //data base case

  private PlayerRepository playerRepository;

  @Autowired
  public PlayerController(PlayerRepository playerRepository) {
    this.playerRepository = playerRepository;
  }

  @RequestMapping(value="/create", method=GET)
  public String showCreatePlayerForm(Model model) {
    model.addAttribute(new PlayerContainer());
    return "createPlayerForm";
  }

  @RequestMapping(value="/create", method=POST)
  public String processCreatePlayer(
          @Valid PlayerContainer playerContainer,
          Errors errors) {
    if (errors.hasErrors()) {
      return "createPlayerForm";
    }

    playerRepository.save(playerContainer);

    return "redirect:/Player/" + playerContainer.getLastName();
  }

//  @RequestMapping(value="/{firstNameAndLastName}", method=GET)
//  public String showCreatedPlayerFirstLastName(@PathVariable String firstNameAndLastName, Model model) {
//
//    String firstName = firstNameAndLastName.split("_")[0];
//    String lastName = firstNameAndLastName.split("_")[1];
//    System.out.println("FN_LN Player: " + firstName + " " + lastName + " " + firstNameAndLastName);
//    PlayerContainer playerContainer = playerRepository.findPlayerByFirstAndLastName(firstName, lastName);
//
//    model.addAttribute(playerContainer);
//
//    System.out.println(model.toString());
//
//    return "showPlayer";
//  }

  @RequestMapping(value = "/{lastName}", method = GET)
  public String showCreatedPlayerLastName(@PathVariable String lastName, Model model) {
    System.out.println();
    System.out.println("LN Player: " + lastName);
    System.out.println();
    List<PlayerContainer> playerContainer = playerRepository.findPlayerByLastName(lastName);
    model.addAttribute("playerContainer", playerContainer);
    System.out.println(model.toString());
    return "showPlayer";
  }

  @RequestMapping(value = "/allPlayers", method = GET)
  public String showCreatedPlayers(Model model) {
    List<PlayerContainer> playerContainer = playerRepository.findAllPlayers();
    model.addAttribute("playerContainer", playerContainer);
    System.out.println(model.toString());
    return "showPlayer";
  }

  @RequestMapping(value = "/displayPlayerUsingREST", method = GET)
  public String getPlayerREST(Model model) {
    model.addAttribute(new PlayerContainer());
    return "showPlayerRest";
  }

  @RequestMapping(value = "/displayPlayerUsingREST", method = POST)
  public String displayPlayerREST(@RequestParam("lastName") String lastName,
                                  Map<String, Object> map, HttpServletRequest request) {
    if ((lastName == "") || (lastName == null)) {
      System.out.println("last names is empty");
      return "showPlayerRest";
    }
    return "redirect:/PlayerREST/" + lastName;
  }
  
}
