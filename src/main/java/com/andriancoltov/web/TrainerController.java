package com.andriancoltov.web;

import com.andriancoltov.beans.Trainer;
import com.andriancoltov.data.TrainerRepository;
import com.andriancoltov.factory.FactoryTrainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/Trainer")
public class TrainerController {

  // non database case
//  private TrainerContainer trainerContainer;
//  private Trainer trainer;
//  private Currency currency = Currency.getInstance(Locale.US);
//
//  @Autowired
//  private FactoryTrainer factoryTrainer;
//
//  @Autowired
//  public TrainerController(TrainerContainer trainerContainer) {
//    this.trainerContainer = trainerContainer;
//  }
//
//  @RequestMapping(value="/create", method=GET)
//  public String showCreateTrainerForm(Model model) {
//    model.addAttribute(new TrainerContainer());
//    return "createTrainerForm";
//  }
//
//  @RequestMapping(value="/create", method=POST)
//  public String processCreateTrainer(
//      @Valid TrainerContainer trainerContainer,
//      Errors errors) {
//    if (errors.hasErrors()) {
//      return "createTrainerForm";
//    }
//
//    trainer = factoryTrainer.createTrainer(trainerContainer.getFirstName(), trainerContainer.getLastName(), trainerContainer.getAge(),
//            trainerContainer.getAnnualSalary(), currency);
//
////    System.out.println(trainer.toString());
//
//    if (trainer == null) {
//      return "createTrainerForm";
//    }
//    else {
//      return "redirect:/Trainer/" + trainer.getFirstName();
//    }
//  }
//
//  @RequestMapping(value="/{firstName}", method=GET)
//  public String showCreatedTrainer(@PathVariable String firstName, Model model) {
//    model.addAttribute(trainer);
//
//    System.out.println(model.toString());
//
//    return "showTrainer";
//  }




  //data base case

  private TrainerRepository trainerRepository;

  @Autowired
  public TrainerController(TrainerRepository trainerRepository) {
    this.trainerRepository = trainerRepository;
  }

  @RequestMapping(value="/create", method=GET)
  public String showCreateTrainerForm(Model model) {
    model.addAttribute(new TrainerContainer());
    return "createTrainerForm";
  }

  @RequestMapping(value="/create", method=POST)
  public String processCreateTrainer(
      @Valid TrainerContainer trainerContainer,
      Errors errors) {
    if (errors.hasErrors()) {
      return "createTrainerForm";
    }

    trainerRepository.save(trainerContainer);

    return "redirect:/Trainer/" + trainerContainer.getFirstName() + "_" + trainerContainer.getLastName();

  }

  @RequestMapping(value="/{firstNameAndLastName}", method=GET)
  public String showCreatedTrainer(@PathVariable String firstNameAndLastName, Model model) {

    String firstName = firstNameAndLastName.split("_")[0];
    String lastName = firstNameAndLastName.split("_")[1];
    System.out.println("FN_LN: " + firstName + " " + lastName + " " + firstNameAndLastName);
    List<TrainerContainer> trainerContainer = trainerRepository.findTrainerByFirstAndLastName(firstName, lastName);
    model.addAttribute("trainerContainer", trainerContainer);
    System.out.println(model.toString());

    return "showTrainer";
  }

  @RequestMapping(value="/allTrainers", method=GET)
  public String showCreatedTrainers(Model model) {
    List<TrainerContainer> trainerContainer = trainerRepository.findAllTrainers();
    model.addAttribute("trainerContainer", trainerContainer);
    System.out.println(model.toString());

    return "showTrainer";
  }

  @RequestMapping(value = "/displayTrainerUsingREST", method = GET)
  public String getTrainerREST(Model model) {
    model.addAttribute(new TrainerContainer());
    return "showTrainerRest";
  }

  @RequestMapping(value = "/displayTrainerUsingREST", method = POST)
  public String displayTrainerREST(@RequestParam("firstName") String firstName,
                                   @RequestParam("lastName") String lastName,
                                   Map<String, Object> map, HttpServletRequest request) {
    if ((firstName == "") || (firstName == null) || (lastName == "") || (lastName == null)) {
      System.out.println("First / Last names are empty");
      return "showTrainerRest";
    }
    return "redirect:/TrainerREST/" + firstName + "_" + lastName;
  }

}
