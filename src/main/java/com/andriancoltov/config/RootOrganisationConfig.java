package com.andriancoltov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.type.filter.RegexPatternTypeFilter;

import java.util.regex.Pattern;

/**
 * Created by Alienware on 2015-07-26.
 */

@Configuration
@Import(DataConfig.class)
@ComponentScan(basePackages = {"com.andriancoltov"},
        excludeFilters={
                @Filter(type= FilterType.CUSTOM, value=RootOrganisationConfig.WebPackage.class)
        })

public class RootOrganisationConfig {

    public static class WebPackage extends RegexPatternTypeFilter {
        public WebPackage() {
            super(Pattern.compile("com\\.andriancoltov\\.web"));
        }
    }
}
