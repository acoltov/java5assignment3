package com.andriancoltov.data;

import com.andriancoltov.beans.Player;
import com.andriancoltov.playerEnumerator.PlayerType;
import com.andriancoltov.web.PlayerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alienware on 2015-08-12.
 */

@Repository
public class JdbcPlayerRepository implements PlayerRepository {

    private JdbcOperations jdbc;

    @Autowired
    public JdbcPlayerRepository(JdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    public PlayerContainer save(PlayerContainer playerContainer) {
        jdbc.update(
          "INSERT INTO Player (first_name, last_name, age, country, position, salary, goals, bookings)" +
                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                playerContainer.getFirstName(),
                playerContainer.getLastName(),
                playerContainer.getAge(),
                playerContainer.getCountryOfBirth(),
                playerContainer.getPosition(),
                playerContainer.getAnnualSalary(),
                playerContainer.getNumberOfGoals(),
                playerContainer.getNumberOfBookings());
        return playerContainer;
    }

    public PlayerContainer findPlayerByFirstAndLastName(String firstName, String lastName) {
        return jdbc.queryForObject(
                "select first_name, last_name, age, country, position, salary, goals, bookings from Player where first_name=? and last_name=?",
                new PlayerContainerRowMapper(),
                firstName, lastName);
    }

    public List<PlayerContainer> findAllPlayers() {
        return jdbc.query(
                "SELECT first_name, last_name, age, country, position, salary, goals, bookings" +
                        " FROM Player",
                new PlayerContainerRowMapper());
    }

    public List<PlayerContainer> findPlayerByLastName(String lastName) {
        try {
            return jdbc.query(
                "select first_name, last_name, age, country, position, salary, goals, bookings from Player where last_name=?",
                new PlayerContainerRowMapper(),
                lastName);
        } catch (EmptyResultDataAccessException e) {
            throw new PlayerNotFoundException(lastName);
        }
    }

    private static class PlayerContainerRowMapper implements RowMapper<PlayerContainer> {
        public PlayerContainer mapRow(ResultSet rs, int rowNum) throws SQLException {

            PlayerType playerType;
            String position = rs.getString("position");
            if (position == "goalkeeper") {
                playerType = PlayerType.goalkeeper;
            }
            else {
                if (position == "defender") {
                    playerType = PlayerType.defender;
                }
                else {
                    if (position == "midfielder") {
                        playerType = PlayerType.midfielder;
                    }
                    else {
                        playerType = PlayerType.forward;
                    }
                }
            }

            PlayerContainer playerContainer = new PlayerContainer(
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getInt("age"),
                    rs.getString("country"),
                    playerType,
                    rs.getBigDecimal("salary"),
                    rs.getInt("goals"),
                    rs.getInt("bookings"));

            return playerContainer;
        }

    }
}
