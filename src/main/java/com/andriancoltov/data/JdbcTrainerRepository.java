package com.andriancoltov.data;

import com.andriancoltov.web.TrainerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alienware on 2015-08-24.
 */

@Repository
public class JdbcTrainerRepository implements TrainerRepository{

    private JdbcOperations jdbc;

    @Autowired
    public JdbcTrainerRepository(JdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    public TrainerContainer save(TrainerContainer trainerContainer) {
        jdbc.update(
                "insert into Trainer (first_name, last_name, age, salary)" +
                        "values (?, ?, ?, ?)",
                trainerContainer.getFirstName(),
                trainerContainer.getLastName(),
                trainerContainer.getAge(),
                trainerContainer.getAnnualSalary());
        return trainerContainer;
    }

    public List<TrainerContainer> findAllTrainers() {
        return jdbc.query(
                "SELECT first_name, last_name, age, salary" +
                        " FROM Trainer",
                new TrainerRowMapper());
    }

    public List<TrainerContainer> findTrainerByFirstAndLastName(String firstName, String lastName) {
        try {
            return jdbc.query(
                "select first_name, last_name, age, salary from Trainer where first_name=? and last_name=?",
                new TrainerRowMapper(),
                firstName, lastName);
        } catch (EmptyResultDataAccessException e) {
            throw new TrainerNotFoundException(firstName, lastName);
        }
    }

    private static class TrainerRowMapper implements RowMapper<TrainerContainer> {
        public TrainerContainer mapRow(ResultSet rs, int rowNum) throws SQLException {
            TrainerContainer trainerContainer = new TrainerContainer(
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getInt("age"),
                    rs.getBigDecimal("salary"));
            return trainerContainer;
        }
    }
}
