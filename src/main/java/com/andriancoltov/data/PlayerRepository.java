package com.andriancoltov.data;

import com.andriancoltov.beans.Player;
import com.andriancoltov.web.PlayerContainer;

import java.util.List;

/**
 * Created by Alienware on 2015-08-12.
 */

public interface PlayerRepository {

    PlayerContainer save(PlayerContainer playerContainer);

    PlayerContainer findPlayerByFirstAndLastName(String firstName, String lastName);

    List<PlayerContainer> findAllPlayers();

    List<PlayerContainer> findPlayerByLastName(String lastName);
}
