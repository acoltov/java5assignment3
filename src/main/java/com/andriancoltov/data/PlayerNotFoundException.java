package com.andriancoltov.data;

/**
 * Created by Alienware.
 */

public class PlayerNotFoundException extends RuntimeException {

    private String lastName;

    public PlayerNotFoundException(String lastName) {
        this.lastName=lastName;
    }

    public String getLastName() {
        return lastName;
    }

}
