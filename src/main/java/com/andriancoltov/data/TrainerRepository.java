package com.andriancoltov.data;

import com.andriancoltov.beans.Trainer;
import com.andriancoltov.web.TrainerContainer;

import java.util.List;

/**
 * Created by Alienware on 2015-08-12.
 */

public interface TrainerRepository {

    TrainerContainer save(TrainerContainer trainerContainer);

    List<TrainerContainer> findAllTrainers();

    List<TrainerContainer> findTrainerByFirstAndLastName(String firstName, String lastName);
}
