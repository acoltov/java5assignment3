package com.andriancoltov.beans;

import com.andriancoltov.interfaces.IPlayer;
import com.andriancoltov.playerEnumerator.PlayerType;

/**
 * Created by Alienware on 2015-07-23.
 */

public class Player implements IPlayer {

    private PlayerType position;

    public Player(PlayerType position) {
        this.position = position;
    }

    public PlayerType getPosition() {
        return position;
    }

}
