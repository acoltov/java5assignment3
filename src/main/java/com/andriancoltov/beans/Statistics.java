package com.andriancoltov.beans;

import com.andriancoltov.interfaces.IStatistics;

import java.util.Objects;

/**
 * Created by Alienware on 2015-07-23.
 */


public class Statistics implements IStatistics {

    private int numberOfGoals = 0;
    private int numberOfBookings = 0;

    public Statistics() {
    }

    public Statistics(int numberOfGoals, int numberOfBookings) {
        this.numberOfGoals = numberOfGoals;
        this.numberOfBookings = numberOfBookings;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }

    public void setNumberOfGoals(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public int getNumberOfBookings() {
        return numberOfBookings;
    }

    public void setNumberOfBookings(int numberOfBookings) {
        this.numberOfBookings = numberOfBookings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Statistics)) return false;
        Statistics that = (Statistics) o;
        return Objects.equals(numberOfGoals, that.numberOfGoals) &&
                Objects.equals(numberOfBookings, that.numberOfBookings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberOfGoals, numberOfBookings);
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "numberOfGoals=" + numberOfGoals +
                ", numberOfBookings=" + numberOfBookings +
                '}';
    }
}
